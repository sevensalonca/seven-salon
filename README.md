We are a new and beautiful farmhouse style salon located in Folsom, CA.

When you visit our salon you can expect a unique experience from any one of our expert stylists, with each visit being personal and relaxing, leaving you feeling pampered.

Address: 187 Blue Ravine Rd, #150, Folsom, CA 95630, USA

Phone: 916-375-9177
